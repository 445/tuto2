package salle;

public class SALLE {

	  private 
	 static int id;
	  String code;
	  String label;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	public String getLabel() {
		return label;
	}

	 public String toString()
	 { 
		 return ("Salle N�: " + (id++) + " de code " + getCode() + " libell� " + getLabel());
	 }
	
}
