package salle;
import java.util.ArrayList;
import java.util.List;

public class SALLE2 implements IDao<SALLE> 
	{

	
	private List<SALLE2> salles;
	 
	public SALLE2() {
		salles = new ArrayList<SALLE>();
	}
 
	@Override
	public boolean create(SALLE o) {
		return salles.add(o);
	}
 
	@Override
	public boolean update(SALLE o) {
		for(SALLE s : salles){
			if(s.getId() == o.getId()){
				s.setCode(o.getCode());
				s.setLibelle(o.getLibelle());
				return true;
			}
		}
		return false;
	}
 
	@Override
	public boolean delete(SALLE o) {
		return salles.remove(o);
	}
 
	@Override
	public List<SALLE> findAll() {
		return salles;
	}
 
	@Override
	public SALLE findById(int id) {
		for (SALLE s : salles) {
			if (s.getId() == id)
				return s;
		}
		return null;
	}

	


}
